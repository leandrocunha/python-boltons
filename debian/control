Source: python-boltons
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders: Leandro Cunha <leandrocunha016@gmail.com>
Section: python
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-all,
               python3-setuptools,
               python3-pytest,
               pybuild-plugin-pyproject,
               flit
Standards-Version: 4.7.0
Vcs-Browser: https://salsa.debian.org/python-team/packages/python-boltons
Vcs-Git: https://salsa.debian.org/python-team/packages/python-boltons.git
Homepage: https://github.com/mahmoud/boltons
Rules-Requires-Root: no

Package: python3-boltons
Architecture: all
Depends: ${misc:Depends},
         ${python3:Depends}
Description: set of pure-Python utilities (Python 3)
 Boltons is a set of over 230 BSD-licensed, pure-Python utilities in the same
 spirit as — and yet conspicuously missing from — the standard library,
 including:
 .
  - Atomic file saving, bolted on with fileutils
  - A highly-optimized OrderedMultiDict, in dictutils
  - Two types of PriorityQueue, in queueutils
  - Chunked and windowed iteration, in iterutils
  - Recursive data structure iteration and merging, with iterutils.remap
  - Exponential backoff functionality, including jitter, through
    iterutils.backoff
  - A full-featured TracebackInfo type, for representing stack traces, in
    tbutils
 .
 This package installs the library for Python 3.
